package br.com.massuda.alexander.spring.framework.infra.web.servico;

import java.io.IOException;

import javax.ws.rs.NotFoundException;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatus.Series;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

import br.com.massuda.alexander.spring.framework.infra.web.excecoes.ErroNegocio;

@Component
public class RestTemplateResponseErrorHandler implements ResponseErrorHandler {
 
    @Override
    public boolean hasError(ClientHttpResponse httpResponse) 
      throws IOException {
 
        return (
          httpResponse.getStatusCode().series() == Series.CLIENT_ERROR 
          || httpResponse.getStatusCode().series() == Series.SERVER_ERROR);
    }
 
    @Override
    public void handleError(ClientHttpResponse httpResponse) 
      throws IOException {
 
        if (httpResponse.getStatusCode().series() == HttpStatus.Series.SERVER_ERROR) {
            // handle SERVER_ERROR
        } else if (httpResponse.getStatusCode().series() == HttpStatus.Series.CLIENT_ERROR) {
        	// handle CLIENT_ERROR
        	if (httpResponse.getStatusCode() == HttpStatus.NOT_FOUND) {
        		throw new NotFoundException();
        	}
//        	byte[] conteudoResposta = null;
//        	httpResponse.getBody().read(conteudoResposta);
//        	String resposta = new String(conteudoResposta);
//        	throw new ErroNegocio(resposta);
        }
    }
}