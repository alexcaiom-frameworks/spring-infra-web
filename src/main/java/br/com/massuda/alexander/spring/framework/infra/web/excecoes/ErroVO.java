package br.com.massuda.alexander.spring.framework.infra.web.excecoes;

import java.io.Serializable;

/**
 * @author Alex
 * Classe Mãe de Erros/Exceções
 */
public class ErroVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 399348359698123046L;
	public String erro;
	public String mensagem;
	
	public ErroVO() {}
	public ErroVO(Erro e){
		this.erro = e.getErro();
	}
	
	public String getErro() {
		return erro;
	}
	public void setErro(String erro) {
		this.erro = erro;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
}