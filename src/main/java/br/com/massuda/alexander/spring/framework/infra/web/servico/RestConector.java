package br.com.massuda.alexander.spring.framework.infra.web.servico;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import javax.annotation.PostConstruct;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import br.com.massuda.alexander.spring.framework.infra.web.dto.Mensagem;
import br.com.massuda.alexander.spring.framework.infra.web.excecoes.Erro;

@Component
public class RestConector<T> extends Conector<T> {

	private Class<T> tipo;
	
//	@PostConstruct
//	public void init() {
//		ParameterizedType genericSuperclass = (ParameterizedType) this.getClass().getGenericSuperclass();
//		Type actualTypeArgument = genericSuperclass.getActualTypeArguments()[0];
//		 this.tipo = (Class<T>) actualTypeArgument.getClass(); //(Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
//	}
	
	/**
	 * Acesso via GET
	 * @param url
	 * @return
	 * @throws Erro 
	 */
	public T getForEntity(String url) throws Erro {
		ResponseEntity<T> resposta = restTemplate.getForEntity(url, tipo);
		return tratarResposta(resposta, url, null, tipo);
	}
	
	/**
	 * 
	 * @param url
	 * @param tipo
	 * @return
	 * @throws Erro 
	 */
	public T getForEntity(String url, Class tipo) throws Erro {
		ResponseEntity<T> resposta = restTemplate.getForEntity(url, tipo);
		return tratarResposta(resposta, url, null, tipo);
	}
	
	public T getForObject(String url) {
		 T list = (T) restTemplate.getForObject(url, tipo);
		return list;
	}
	
	public Object getForObject(String url, Class tipo) {
		Object obj = restTemplate.getForObject(url, tipo);
		return obj;
	}
	
	/**
	 * 
	 * @param url
	 * @param o
	 * @param tipo
	 * @return
	 */
	public T postForEntity(String url, T o, Class<T> tipo) throws Erro {
		ResponseEntity<T> resposta = restTemplate.postForEntity(url, o, tipo);
		return tratarResposta(resposta, url, o, tipo);
	}
	
	/**
	 * 
	 * @param url
	 * @param o
	 * @return
	 * @throws Erro 
	 */
	public T postForEntity(String url, T o) throws Erro {
		ResponseEntity<T> resposta = restTemplate.postForEntity(url, o, tipo);
		return tratarResposta(resposta,url, o, tipo);
	}

	private boolean sucesso(ResponseEntity<T> resposta) {
		return resposta.getStatusCode().is2xxSuccessful() ||
				resposta.getStatusCode().is1xxInformational();
	}
	
	private T tratarResposta(ResponseEntity<T> resposta, String url, T o, Class tipo2) throws Erro {
		if (!sucesso(resposta)) {
			tratarHttpStatusErro(resposta, url, o, tipo);
		}
		return resposta.getBody();
	}
	
	private void tratarHttpStatusErro(ResponseEntity resposta, String url, T o, Class tipo2) throws Erro {
		Erro erro = null;
		try {
			erro = (Erro) resposta.getBody();
			
		}catch (ClassCastException e) {
			Mensagem mensagem = restTemplate.postForEntity(url, o, Mensagem.class).getBody();
			erro = new Erro(mensagem.getMensagem());
		}
		throw erro;
	}

	public Class<T> getTipo() {
		return tipo;
	}

	public void setTipo(Class<T> tipo) {
		this.tipo = tipo;
	}
	
	
}
