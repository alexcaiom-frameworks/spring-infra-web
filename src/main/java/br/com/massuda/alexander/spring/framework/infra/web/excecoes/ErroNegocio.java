package br.com.massuda.alexander.spring.framework.infra.web.excecoes;

import java.io.Serializable;

/**
 * @author Alex
 *
 */
@SuppressWarnings("serial")
public class ErroNegocio extends Erro implements Serializable {

	public ErroNegocio() {
		// TODO Auto-generated constructor stub
	}
	
	public ErroNegocio(String erro) {
		super(erro);
	}
	
}