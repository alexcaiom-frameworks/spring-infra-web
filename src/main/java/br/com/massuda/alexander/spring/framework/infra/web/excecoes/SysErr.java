package br.com.massuda.alexander.spring.framework.infra.web.excecoes;

import java.io.Serializable;


/**
 * @author Alex
 *
 */
public class SysErr extends Erro implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7304609136575584725L;

	
	public SysErr(Throwable erro, String descricaoErro) {
		super(descricaoErro);
		super.initCause(new Exception(erro));
	}
	
	public SysErr(String descricaoErro) {
		super(descricaoErro);
	}
	
}