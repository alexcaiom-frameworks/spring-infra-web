/**
 * 
 */
package br.com.massuda.alexander.spring.framework.infra.web.servico;

//import javax.ws.rs.core.Response;
//import javax.ws.rs.core.Response.StatusType;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestClientException;

import br.com.massuda.alexander.spring.framework.infra.web.excecoes.ErroVO;
import br.com.waiso.framework.exceptions.ErroNegocio;

/**
 * @author Alex
 *
 */
public class ServicoRestUtils<T> {

	/**
	 * isError
	 * @param resposta
	 * @return
	 */
	protected boolean ehErro(ResponseEntity resposta) {
		HttpStatus statusCode = resposta.getStatusCode();
//		HttpStatus statusHTTP = HttpStatus.valueOf(statusCode.);
		return statusCode.is4xxClientError() || statusCode.is5xxServerError();
	}
	
	/**
	 * Treat errors
	 * @param resposta
	 */
	protected void tratarErro(ResponseEntity resposta) {
		HttpStatus statusCode = resposta.getStatusCode();
		ErroVO mensagemServidor = (ErroVO) resposta.getBody();//resposta.readEntity(String.class);
		switch (statusCode.series()) {
			case CLIENT_ERROR:
				if (StringUtils.isEmpty(mensagemServidor)) {
					throw new HttpClientErrorException(statusCode,  resposta.getStatusCode().getReasonPhrase());
				} else {
					throw new ErroNegocio(mensagemServidor.getMensagem());
				}
			case SERVER_ERROR:
				throw new HttpServerErrorException(statusCode, StringUtils.isEmpty(mensagemServidor.getMensagem()) ? resposta.getStatusCode().getReasonPhrase() : mensagemServidor.getMensagem());
			default:
				throw new RestClientException("Unknown status code [" + statusCode + "]");
		}
	}

}
