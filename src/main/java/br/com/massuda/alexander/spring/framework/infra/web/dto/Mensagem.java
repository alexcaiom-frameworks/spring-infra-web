/**
 * 
 */
package br.com.massuda.alexander.spring.framework.infra.web.dto;

import java.io.Serializable;

import br.com.massuda.alexander.spring.framework.infra.web.excecoes.Erro;

/**
 * @author Alex
 *
 */
public class Mensagem implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6144488911307707958L;
	private String codigo;
	private String mensagem;
	private Exception e;
	
	public Mensagem(Erro e) {
		this.e = e;
		this.mensagem = e.getMensagem();
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public Exception getE() {
		return e;
	}
	public void setE(Exception e) {
		this.e = e;
	}

}
